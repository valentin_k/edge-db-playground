# README #

### Что ###

* Твиттер на EdgeDB + FastAPI.

### Как ###

* ```docker-compose up -d```
* после первого запуска можно закомментировать ```migrate_drop``` и ```migrate``` в функции ```startup_event``` (не стал утруждать себя приделыванием нормальных миграций)
* http://127.0.0.1:8000/docs - свагер

### Зачем ###

* https://edgedb.com/blog/edgedb-1-0-alpha-1/
* Как минимум, это любопытно синтаксически. Допустим, мы хотим вывести страницу пользователя, список фоловеров и объединенную ленту, состоящую из собственных постов пользователя и постов тех, на кого пользователь подписан:

Раз..

```
SELECT User {
    id,
    name,
    email,
    tweets := (
        .<author
        UNION
        .followees[IS User].<author
    )[IS Tweet] {
        id,
        author: {
            id,
            name
        },
        text,
        likes := count(.liked_by),
        created
    },
    followed_by := .<followees[IS User] {
        id,
        name
    }
}
FILTER .id = <uuid>$id
```

..и сразу в json, даже сериализатор не нужен:

```javascript
{
    "id":"e1278e98-edd2-11ea-9149-e76fdf3bce67",
    "name":"Petya",
    "email":"user@example.ru",
    "tweets":[
        {
            "id":"0bfdb28c-edd3-11ea-9149-b7c2de426b50",
            "author":{
                "id":"d6f4f078-edd2-11ea-9149-e3333909a9ef",
                "name":"Vasya"
            },
            "text":"Hello from Vasya!",
            "likes":1,
            "created":"2020-09-03T10:48:48.327886+00:00"
        },
        {
            "id":"136a1614-edd3-11ea-9149-8f3d2da20c21",
            "author":{
                "id":"d6f4f078-edd2-11ea-9149-e3333909a9ef",
                "name":"Vasya"
            },
            "text":"And hello again!",
            "likes":1,
            "created":"2020-09-03T10:49:00.78227+00:00"
        },
        {
            "id":"29f1ead8-edd3-11ea-9149-bb1cb8f42943",
            "author":{
                "id":"e1278e98-edd2-11ea-9149-e76fdf3bce67",
                "name":"Petya"
            },
            "text":"Petya can use twitter too!",
            "likes":0,
            "created":"2020-09-03T10:49:38.582311+00:00"
        },
        {
            "id":"5def3ec4-edd5-11ea-8b72-e3b6be03d4e4",
            "author":{
                "id":"d6f4f078-edd2-11ea-9149-e3333909a9ef",
                "name":"Vasya"
            },
            "text":"My last tweet!!",
            "likes":0,
            "created":"2020-09-03T11:05:24.799797+00:00"
        }
    ],
    "followed_by":[
        {
            "id":"c6772b18-edd6-11ea-8b72-1b07bcad84f0",
            "name":"Kolya"
        }
    ]
}
```
