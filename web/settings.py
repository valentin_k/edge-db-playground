import os
from secrets import token_urlsafe


DATABASE_USER = os.environ.get('EDGEDB_USER', 'edgedb')
SECRET = token_urlsafe(32)
