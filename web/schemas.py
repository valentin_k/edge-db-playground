import uuid
from datetime import datetime
from typing import Optional

from fastapi_users import models
from pydantic import UUID1, validator


class User(models.BaseUser):
    id: Optional[UUID1] = None
    name: Optional[str] = None
    registered: Optional[datetime] = None

    @validator('id', pre=True, always=True)
    def default_id(cls, v):
        return v or uuid.uuid1()


class UserCreate(models.BaseUserCreate):
    id: Optional[UUID1] = None
    name: Optional[str] = None

    @validator('id', pre=True, always=True)
    def default_id(cls, v):
        return v or uuid.uuid1()


class UserUpdate(User, models.BaseUserUpdate):
    id: Optional[UUID1] = None


class BaseUserDB(models.BaseUserDB):
    id: UUID1

    @validator('id', pre=True, always=True)
    def default_id(cls, v):
        return v or uuid.uuid1()


class UserDB(User, BaseUserDB):
    id: Optional[UUID1] = None

    @validator('id', pre=True, always=True)
    def default_id(cls, v):
        return v or uuid.uuid1()
