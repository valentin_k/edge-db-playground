from typing import Optional, Type
import edgedb
from fastapi_users import FastAPIUsers
from fastapi_users.db.base import BaseUserDatabase
from fastapi_users.models import UD
from pydantic import UUID1
from schemas import User, UserCreate, UserDB, UserUpdate
from fastapi_users.authentication import JWTAuthentication
import settings


class EdgeUserAdapter(BaseUserDatabase[UD]):
    def __init__(
        self,
        user_db_model: Type[UD],
    ):
        super().__init__(user_db_model)

    def add_pool(self, pool: edgedb.AsyncIOPool):
        self.pool = pool

    async def get(self, id: UUID1) -> Optional[UD]:
        async with self.pool.acquire() as connection:
            users = await connection.query(
                '''
                SELECT User {
                    id,
                    email,
                    name,
                    hashed_password,
                    is_active,
                    is_superuser,
                    registered
                }
                FILTER .id = <uuid>$id
                ''',
                id=id
            )

        return await self._make_user(users[0]) if users else None

    async def get_by_email(self, email: str) -> Optional[UD]:
        async with self.pool.acquire() as connection:
            users = await connection.query(
                '''
                SELECT User {
                    id,
                    email,
                    name,
                    hashed_password,
                    is_active,
                    is_superuser,
                    registered
                }
                FILTER str_lower(.email) = str_lower(<str>$email)
                ''',
                email=email
            )

        return await self._make_user(users[0]) if users else None

    async def create(self, user: UD) -> UD:
        user_dict = user.dict()
        user_dict.pop('id', None)
        user_dict.pop('registered', None)

        async with self.pool.acquire() as connection:
            await connection.query(
                '''
                INSERT User {
                    email := <str>$email,
                    name := <str>$name,
                    hashed_password := <str>$hashed_password,
                    is_active := <bool>$is_active,
                    is_superuser := <bool>$is_superuser,
                };
                ''',
                **user_dict
            )

        return user

    async def update(self, user: UD) -> UD:
        user_dict = user.dict()
        user_dict['id'] = user.id

        async with self.pool.acquire() as connection:
            await connection.query(
                '''
                UPDATE User
                FILTER .id = <uuid>$id
                SET {
                    email := <OPTIONAL str>$email,
                    name := <OPTIONAL str>$name,
                    hashed_password := <OPTIONAL str>$hashed_password,
                    is_active := <OPTIONAL bool>$is_active,
                    is_superuser := <OPTIONAL bool>$is_superuser,
                };
                ''',
                **user_dict
            )

        return user

    async def delete(self, user: UD) -> None:
        async with self.pool.acquire() as connection:
            await connection.query(
                '''
                DELETE User
                FILTER .id = <uuid>$id
                ;
                ''',
                id=user.id
            )

    async def _make_user(self, user) -> UD:
        user_dict = {}

        for attr in (
            'id',
            'email',
            'name',
            'hashed_password',
            'is_active',
            'is_superuser',
            'registered',
        ):
            if (value := getattr(user, attr, None)) is not None:
                user_dict[attr] = value

        return self.user_db_model(**user_dict)


jwt_authentication = JWTAuthentication(
    secret=settings.SECRET, lifetime_seconds=3600, tokenUrl='/auth/jwt/login'
)

user_adapter = EdgeUserAdapter(
    UserDB
)

fastapi_users_adapter = FastAPIUsers(
    user_adapter,
    [jwt_authentication],
    User,
    UserCreate,
    UserUpdate,
    UserDB
)
