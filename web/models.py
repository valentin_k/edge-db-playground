import asyncio

import edgedb
import settings


async def migrate(pool):
    async with pool.acquire() as connection:
        await connection.execute('''
            START MIGRATION TO {
                module default {
                    type User {
                        required property email -> str {
                            constraint exclusive;
                        };
                        required property name -> str {
                            constraint max_len_value(32);
                            # TODO
                            # fastapi-users не может поддерживать
                            # уникальность доп полей без костылей
                            # надо наверное отдельную
                            # таблицу с профилем пользователя
                            # constraint exclusive;
                        }
                        required property hashed_password -> str;
                        property is_active -> bool {
                            default := true;
                        };
                        property is_superuser -> bool {
                            default := false;
                        };
                        required property registered -> datetime {
                            default := datetime_current();
                        };
                        property gender -> str {
                            constraint one_of('Male', 'Female', 'Other');
                            default := 'Other';
                        };
                        multi link followees -> User;
                        index on (str_lower(.email));
                        # TODO
                        # нужен ли индекс на айди или он и так есть?
                    };

                    type Tweet {
                        required property text -> str {
                            constraint max_len_value(512);
                        };
                        required link author -> User;
                        required property created -> datetime{
                            default := datetime_current();
                        };
                        multi link liked_by -> User;
                    };
                };
            };
            POPULATE MIGRATION;
            COMMIT MIGRATION;
        ''')


async def migrate_drop(pool):
    async with pool.acquire() as connection:
        await connection.execute('''
            START MIGRATION TO {
                module default {
                };
            };
            POPULATE MIGRATION;
            COMMIT MIGRATION;
        ''')


class PoolSingleton():
    _pool = None

    @classmethod
    async def create_pool(cls):
        cls._pool = await edgedb.create_async_pool(
            host='db',
            port=5656,
            user=settings.DATABASE_USER,
        )

    @classmethod
    def pool(cls):
        return cls._pool

    @classmethod
    async def close_pool(cls):
        try:
            await asyncio.wait_for(
                cls._pool.aclose(),
                timeout=5
            )
        except asyncio.TimeoutError:
            cls._pool.terminate()
