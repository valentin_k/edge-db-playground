import uuid

import edgedb
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import Response
from models import PoolSingleton
from pydantic import UUID1
from schemas import User
from users import fastapi_users_adapter

router = APIRouter()


@router.get('/test')
async def test():
    return {'message': 'Hello World'}


@router.get('/profiles')
async def profiles():
    # TODO
    # вот это точно надо вынести в зависимость
    pool = PoolSingleton.pool()

    async with pool.acquire() as connection:
        try:
            result = await connection.query_json(
                '''
                SELECT User {
                    id,
                    registered,
                    name,
                    email,
                    tweets_count := count(.<author),
                    followed_by_count := count(.<followees)
                }
                '''
            )
        except edgedb.errors.NoDataError:
            raise HTTPException(status_code=404, detail='Item not found')

    return Response(content=result, media_type='application/json')


@router.get('/page/{user_id}')
async def user_page(
    # followed_included
    user_id: uuid.UUID,
    user: User = Depends(fastapi_users_adapter.get_optional_current_user),
):
    pool = PoolSingleton.pool()

    async with pool.acquire() as connection:
        try:
            result = await connection.query_one_json(
                '''
                SELECT User {
                    id,
                    name,
                    email,
                    tweets := (
                        .<author
                        UNION
                        .followees[IS User].<author
                    )[IS Tweet] {
                        id,
                        author: {
                            id,
                            name
                        },
                        text,
                        likes := count(.liked_by),
                        created
                    },
                    followed_by := User.<followees[IS User] {
                        id,
                        name
                    }
                }
                FILTER .id = <uuid>$id
                ''',
                id=user_id
            )
        except edgedb.errors.NoDataError:
            raise HTTPException(status_code=404, detail='Item not found')

    return Response(content=result, media_type='application/json')


@router.post('/post_tweet/')
async def post_tweet(
    tweet: str,
    user: User = Depends(fastapi_users_adapter.get_current_user)
):
    pool = PoolSingleton.pool()

    async with pool.acquire() as connection:
        result = await connection.query_json(
            '''
            WITH MODULE default
            INSERT Tweet {
                text := <str>$tweet,
                author := (
                    SELECT default::User
                    FILTER User.id = <uuid>$id
                )
            };
            ''',
            tweet=tweet,
            id=user.id
        )

    return Response(content=result, media_type='application/json')


@router.post('/like_tweet')
async def like_tweet(
    tweet_id: UUID1,
    user: User = Depends(fastapi_users_adapter.get_current_user)
):
    pool = PoolSingleton.pool()

    async with pool.acquire() as connection:
        result = await connection.query_json(
            '''
            UPDATE Tweet
            FILTER
                Tweet.id = <uuid>$tweet_id
            SET {
                liked_by += {(
                    SELECT User
                    FILTER
                        User.id = <uuid>$user_id
                )}
            };
            ''',
            tweet_id=tweet_id,
            user_id=user.id
        )
        if not result:
            raise HTTPException(status_code=404, detail='Tweet not found')

    return Response(content=result, media_type='application/json')


@router.post('/follow_user')
async def follow_user(
    # TODO
    # возможно стоит вынести другого пользователя в зависимость
    # по аналогии с текущим юзером?
    # но это ведь лишний запрос в БД
    user_id: UUID1,
    user: User = Depends(fastapi_users_adapter.get_current_user)
):
    pool = PoolSingleton.pool()

    if user_id == user.id:
        raise HTTPException(status_code=400, detail='Cant follow yourself')

    async with pool.acquire() as connection:
        result = await connection.query_json(
            '''
            UPDATE User
            FILTER
                .id = <uuid>$user_id
            SET {
                followees += {(
                    SELECT DETACHED User
                    FILTER
                        .id = <uuid>$target_user_id
                )}
            };
            ''',
            target_user_id=user_id,
            user_id=user.id
        )
        if not result:
            raise HTTPException(status_code=404, detail='User not found')

    return Response(content=result, media_type='application/json')
