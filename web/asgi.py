import settings
from fastapi import FastAPI, Request
from models import PoolSingleton
from routers import twitter
from schemas import UserDB
from users import fastapi_users_adapter, jwt_authentication, user_adapter


def on_after_register(user: UserDB, request: Request):
    print(f'User {user.id} has registered.')


def on_after_forgot_password(user: UserDB, token: str, request: Request):
    print(f'User {user.id} has forgot their password. Reset token: {token}')


app = FastAPI()


@app.on_event('startup')
async def startup_event():
    await PoolSingleton.create_pool()
    pool = PoolSingleton.pool()

    from models import migrate_drop
    await migrate_drop(pool)

    from models import migrate
    await migrate(pool)

    user_adapter.add_pool(pool)


@app.on_event('shutdown')
async def shutdown_event():
    await PoolSingleton.close_pool()


app.include_router(
    fastapi_users_adapter.get_auth_router(jwt_authentication),
    prefix='/auth/jwt',
    tags=['auth']
)
app.include_router(
    fastapi_users_adapter.get_register_router(on_after_register),
    prefix='/auth',
    tags=['auth']
)
app.include_router(
    fastapi_users_adapter.get_reset_password_router(
        settings.SECRET,
        after_forgot_password=on_after_forgot_password
    ),
    prefix='/auth',
    tags=['auth'],
)
app.include_router(
    fastapi_users_adapter.get_users_router(),
    prefix='/users',
    tags=['users']
)
app.include_router(
    twitter.router,
    prefix="/twitter",
    tags=["twitter"],
    # dependencies=[Depends(get_pool)],
    responses={404: {"description": "Not found"}},
)
